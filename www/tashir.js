var exec = require('cordova/exec');
var platform = require('cordova/platform');

module.exports = {
    scan: function(success, fail, permanent) {
        exec(success, fail, "Tashir", "scan", [permanent || false]);
    },
    setTime: function(time) {
        exec(null, null, "Tashir", "set-time", [time]);
    },
    getTime: function(success, fail) {
        exec(success, fail, "Tashir", "get-time", []);
    },
};
