package org.tashir;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONArray;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.net.Uri;
import android.app.Activity;


public class Tashir extends CordovaPlugin {
    final String LOG_TAG = "tashir";


    protected void pluginInitialize() {
        Helper.setActivity(this.cordova.getActivity());
        Helper.initDB();
        Log.d(LOG_TAG, " init plugin ");
    }

    private CallbackContext lastScanCallbackContext = null;

    @Override
    public boolean execute(final String action, final JSONArray data, CallbackContext callbackContext) throws JSONException {
        PluginResult pr = new PluginResult(PluginResult.Status.ERROR);
        if (action.equals("scan")) {
            try {
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                this.cordova.startActivityForResult((CordovaPlugin) this, intent, 0);
            } catch (Exception e) {
                Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
                Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
                this.cordova.startActivityForResult((CordovaPlugin) this, marketIntent, 1);
            }
            pr = new PluginResult(PluginResult.Status.NO_RESULT);
            pr.setKeepCallback(true);
            lastScanCallbackContext = callbackContext;
        } else if (action.equals("set-time")) {
            Helper.setTime(data.getLong(0) * 1000);
            pr = new PluginResult(PluginResult.Status.OK);
        } else if (action.equals("get-time")) {
            pr = new PluginResult(PluginResult.Status.OK, "" + Helper.getTime() / 1000);
        }
        callbackContext.sendPluginResult(pr);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && lastScanCallbackContext != null) {
            if (resultCode == Activity.RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                lastScanCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, contents));
                Log.d(LOG_TAG, "scan: " + contents);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                lastScanCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
                Log.d(LOG_TAG, "cancel");
            }
            lastScanCallbackContext = null;
        }
    }
}