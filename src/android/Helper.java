package org.tashir;

import android.content.Context;
import android.app.Activity;
import org.apache.cordova.CordovaWebView;
import android.os.SystemClock;
import android.util.Log;
import org.tashir.data.DBHelper;

import android.telephony.TelephonyManager;
import java.util.UUID;

public class Helper {
    final static String LOG_TAG = "tashir helper";

    private static DBHelper dbHelper = null;
    public static void initDB() {
        dbHelper = new DBHelper(getActivity());
    }
    public static DBHelper getDbHelper() {
        return dbHelper;
    }

    private static Activity activity;
    public static void setActivity(Activity a) {
        activity = a;
    }
    public static Activity getActivity() {
        return activity;
    }

    private static long serverTime = 0;
    private static long rebootTime = 0;
    public static void setTime(long st) {
        serverTime = st;
        rebootTime = SystemClock.elapsedRealtime();
        dbHelper.writeTime(serverTime, rebootTime);
    }
    public static long getTime() {
        if (serverTime == 0) {
            serverTime = dbHelper.readServerTime();
            rebootTime = dbHelper.readLocalTime();
        }
        long currentRebootTime = SystemClock.elapsedRealtime();
        if (currentRebootTime < rebootTime) {
            rebootTime = 0;
            serverTime = 0;
        }
        if (serverTime > 0) {
            return currentRebootTime - rebootTime + serverTime;
        } else {
            return System.currentTimeMillis();
        }
    }
}