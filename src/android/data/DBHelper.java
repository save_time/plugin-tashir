package org.tashir.data;

import org.tashir.Helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.content.ContentValues;
import android.location.Location;

import android.content.IntentFilter;
import android.content.Intent;
import android.os.BatteryManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DBHelper extends SQLiteOpenHelper {

    final int DB_VERSION = 1;
    final String LOG_TAG = "mdsp dbHelper";

    public DBHelper(Context context) {
        super(context, "mdsp-track", null, 1);
    }

    public void clearAll() {
        this.onCreate(this.getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "--- onCreate database ---");
        db.execSQL("DROP TABLE IF EXISTS dove");
        db.execSQL("CREATE TABLE IF NOT EXISTS `dove` (" +
                        "`key` text primary key," +
                        "`value` text" +
                    ");");
    }

    public void onStart() {
        onCreate(this.getWritableDatabase());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    public void writeTime(long serverTime, long localTime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("key", "server-time");
        cv.put("value", "" + serverTime);
        db.replace("dove", null, cv);

        cv = new ContentValues();
        cv.put("key", "local-time");
        cv.put("value", "" + localTime);
        db.replace("dove", null, cv);
    }
    public long readServerTime() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT `value` FROM `dove` WHERE `key` = 'server-time'", null);
        long value = 0;
        if (c.moveToFirst()) {
            value = c.getLong(c.getColumnIndex("value"));
        }
        c.close();
        return value;
    }
    public long readLocalTime() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT `value` FROM `dove` WHERE `key` = 'local-time'", null);
        long value = 0;
        if (c.moveToFirst()) {
            value = c.getLong(c.getColumnIndex("value"));
        }
        c.close();
        return value;
    }
}